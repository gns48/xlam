#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <cctype>
#include <locale>
#include <codecvt>
#include <set>
#include <unordered_map>
#include <vector>
#include <array>
#include <cmath>

/* letter frequencies for the Russian language */
static const double pxi[32] = {
 /* а */ .0801,
 /* б */ .0159,
 /* в */ .0454,
 /* г */ .0170,
 /* д */ .0298,
 /* е */ .0849,
 /* ж */ .0094,
 /* з */ .0165,
 /* и */ .0735,
 /* й */ .0121,
 /* к */ .0349,
 /* л */ .0440,
 /* м */ .0321,
 /* н */ .0670,
 /* о */ .1097,
 /* п */ .0281,
 /* р */ .0473,
 /* с */ .0547,
 /* т */ .0626,
 /* у */ .0262,
 /* ф */ .0026,
 /* х */ .0097,
 /* ц */ .0048,
 /* ч */ .0144,
 /* ш */ .0073,
 /* щ */ .0036,
 /* ъ */ .0004,
 /* ы */ .0190,
 /* ь */ .0174,
 /* э */ .0032,
 /* ю */ .0064,
 /* я */ .0201,
};

int read_input(std::string &input, std::string &alphabet, const char *fname)
{
    std::ifstream ifs(fname);
    if(!ifs) {
        std::cerr << "Can not open  "  << fname << std::endl;
        return EINVAL;
    }
    getline(ifs, alphabet);

    while(!ifs.eof()) {
        std::string line;
        getline(ifs, line);
        input.append(line);
    }
    ifs.close();

    input.erase(remove_if(input.begin(), input.end(), isspace), input.end());
    return 0;
}

static std::wstring_convert <std::codecvt_utf8_utf16<char16_t>, char16_t> c816 {};

static inline std::u16string convert_8_to_16(const std::string& s8) {
    return c816.from_bytes(s8.data());
}

static inline std::string convert_16_to_8(const std::u16string& s16) {
    return c816.to_bytes(s16.data());
}

static bool find_as_substr(const std::u16string& ps,
                           std::vector<std::u16string>& v)
{
    for (int i = 0; i < v.size(); i++) {
        if ((v[i].find(ps) != std::u16string::npos) &&
            (v[i].size() > ps.size()))
        {
            std::cout << convert_16_to_8(ps) << " found in "
                      << convert_16_to_8(v[i]) << '\n';
            return true;
        }
    }

    return false;
}

static void PrimeFactors(int n, std::set<int>& result) {
    for (int i = 2; i * i <= n; i += 1 + (i > 2)) {
        while ((n % i) == 0) {
            result.insert(i);
            n /= i;
        }
    }
    if (n != 1) result.insert(n);
}

static int gcd(int a, int b) {
	if (a < b) return gcd(b, a);
	else {
		int r = a % b;
		if (r == 0) return b;
		else return gcd(b, r);
	}
}

static int find_keylen_by_offsets(const std::u16string& cipher,
								  int minlen, int maxlen)
{
    std::unordered_map<std::u16string, std::set<int>> pattern_offsets;
    std::vector<std::u16string> all_patterns;
    std::vector<std::u16string> uniq_patterns;
    std::set<int> distances;
    std::set<int> factors;

    for (int substr_len = maxlen; substr_len >= minlen; substr_len--) {
        for(ssize_t pos = 0; pos < cipher.size() - substr_len; pos++) {
            std::u16string pattern = cipher.substr(pos, substr_len);
            std::u16string::size_type found = pos + substr_len;

            pattern_offsets[pattern].insert(pos);

            while(1) {
                found = cipher.find(pattern, found);
                if (found == std::u16string::npos) break;
                pattern_offsets[pattern].insert(found);
                found += substr_len;
            }
        }
    }

    for (auto const& [pt, offsets] : pattern_offsets) {
        if (offsets.size() >= 2) all_patterns.push_back(pt);
    }

    for (int i = 0; i < all_patterns.size(); i++) {
        bool uniq = true;
        for (int j = 0; j < all_patterns.size(); j++) {
            if ((all_patterns[j].find(all_patterns[i]) != std::u16string::npos) &&
                (all_patterns[j].size() > all_patterns[i].size()))
            {
                /*
                  std::cout << convert_16_to_8(all_patterns[i])
                            << " found in "
                            << convert_16_to_8(all_patterns[j]) << '\n';
                */
                uniq = false;
                break;
            }
        }
        if (uniq)
            uniq_patterns.push_back(all_patterns[i]);
    }

    for (int i = 0; i < uniq_patterns.size(); i++) {
        std::vector<int> dist;
        std::cout << convert_16_to_8(uniq_patterns[i]) << ":";
        for (auto const ofs : pattern_offsets[uniq_patterns[i]]) {
            dist.push_back(ofs);
            std::cout << "\x20" << ofs;
        }
        std::cout << '\n';
        for (int d = 0; d < dist.size(); d++) {
            for (int e = 0; e < dist.size(); e++) {
                int res = abs(dist[d] - dist[e]);
                if (res != 0) distances.insert(res);
            }
        }
    }

    std::cout << "Distanses:";
    for (auto d : distances) {
        PrimeFactors(d, factors);
        std::cout << "\x20" << d;
    }
    std::cout << '\n';

    std::cout << "Prime Factors & Remainders:\n";
    int best_factor = 0;
    int max_zeroes = 0;
    for (auto f : factors) {
        std::cout << f << ":";
        int zerocount = 0;
        for (auto d : distances) {
            int res = d % f;
            std::cout << " " << res;
            if (res == 0) zerocount++;
        }
        std::cout << " #of zeros: " << zerocount << '\n';
        if (zerocount > max_zeroes) {
            max_zeroes = zerocount;
            best_factor = f;
        }
    }

    std::cout << "Best distance: " << best_factor << '\n';

    return best_factor;
}

inline void cipher_toint(const std::u16string& alphabet,
                         const std::u16string& cipher,
                         std::vector<int>& intcipher)
{
    for (auto l : cipher) {
        intcipher.push_back(alphabet.find(l));
    }
}

#define KW 16

int find_keylen_by_ic(const std::vector<int>& cipher,
					  const std::u16string& alphabet)
{
	std::cout << "Cipher length: " << cipher.size() << '\n';
	std::unordered_map<int, int> groups[KW];
	int counts[KW];
	double ic[KW];
	double ic_means[KW];
	int maxkwlen = 0;

	for (int kwlen = 1; kwlen < KW; kwlen++) {
		std::cout << "\nkwlen: " << kwlen << '\n';
		int start;
		ic_means[kwlen] = 0.0;

		for (start = 0; start < kwlen; start++) {
			groups[start].clear();
			counts[start] = 0;
			ic[start] = 0.0;

			int pos = start;
			while (pos < cipher.size()) {
				groups[start][cipher[pos]]++;
				counts[start]++;
				pos += kwlen;
			}

			for (auto [l, count] : groups[start]) {
				ic[start] += (double)(count * (count - 1));
			}
			ic[start] /= counts[start] * (counts[start] - 1);
			std::cout << "ic[" << start << "] = " << ic[start] << '\n';
		}

		for (start = 0; start < kwlen; start++) {
			ic_means[kwlen-1] += ic[start];
		}
		ic_means[kwlen-1] /= kwlen;
		std::cout << "ic mean = " << ic_means[kwlen-1] << '\n';

		for (int l = 0; l < alphabet.size(); l++) {
			for (int group = 0; group < kwlen; group++) {
				std::cout << convert_16_to_8(alphabet.substr(l,1))
						  << ": " << groups[group][l] << "  ";
			}
			std::cout << '\n';
		}
	}

	double maxic = 0.0;
	for (int kwlen = 0; kwlen < KW; kwlen++) {
		if (ic_means[kwlen] > maxic)
			maxic = ic_means[kwlen], maxkwlen = kwlen+1;
	}
	std::cout << "Max mean ic: " << maxic << '\n';
	std::cout << "kwlen: " << maxkwlen << '\n';

    return maxkwlen;
}


int main(int ac, const char **av) {
    std::string raw_input;
    std::string raw_alphabet;

    const char* lang = getenv("LANG");
    std::setlocale(LC_ALL, lang);

    if(ac < 2) {
        std::cerr << "Usage: " << av[0] << " <file with cipher>" << '\n';
        return EINVAL;
    }

    int rv = read_input(raw_input, raw_alphabet, av[1]);
    if(rv) return rv;

    std::u16string alphabet = convert_8_to_16(raw_alphabet);
    std::u16string cipher = convert_8_to_16(raw_input);
    std::vector<int> intcipher;

    cipher_toint(alphabet, cipher, intcipher);

    int keylen1 = find_keylen_by_offsets(cipher, 3, 10);
    int keylen2 = find_keylen_by_ic(intcipher, alphabet);
	int keylen = gcd(keylen1, keylen2);

	std::cout << "keylen by offsets: " << keylen1 << '\n'
			  << "keylen by ic: " << keylen2 << '\n'
			  << "gcd keylen: " << keylen << '\n';

	find_alphabeth(keylen, intcipher)

    return 0;
}
