#include <map>
#include <algorithm>
#include <iostream>

/*
  Cnk = n! / (k! * (n-k)!)
*/

typedef std::unordered_map<unsigned long long, int> fcmap_t;

static void primeFactorsMap(unsigned long long n, fcmap_t& factors) {
    for (unsigned long long i = 2; i * i < n; i += 1 + (i > 2)) {
        while ((n % i) == 0) {
            factors[i]++;
            n /= i;
        }
    }
    if (n != 1) factors[n]++;
}

double Cnk(unsigned long long n, unsigned long long k) {
    fcmap_t nom_factors;
    fcmap_t denom_factors;

    unsigned long long i;
    if (k > (n - k)) {
        for (i = k+1; i <= n; i++)
            primeFactorsMap(i, nom_factors);
        for (i = 2; i <= n-k; i++)
            primeFactorsMap(i, denom_factors);
    }
    else {
        for (i = n-k+1; i <= n; i++)
            primeFactorsMap(i, nom_factors);
         for (i = 2; i <= k; i++)
            primeFactorsMap(i, denom_factors);
    }

    double nom = 1.0;
    double denom = 1.0;

    for (auto &[f, p] : nom_factors) {
        if ((f == 1) || (p == 0)) continue;
        fcmap_t::iterator e = denom_factors.find(f);
        if (e != denom_factors.end()) {
            int m = std::min(p, e->second);
            nom_factors[f] -= m;
            denom_factors[f] -= m;
        }
    }
    for (auto &[f, p] : denom_factors) {
        if ((f == 1) || (p == 0)) continue;
        fcmap_t::iterator e = nom_factors.find(f);
        if (e != nom_factors.end()) {
            int m = std::min(p, e->second);
            nom_factors[f] -= m;
            denom_factors[f] -= m;
        }
    }

    for (auto &[f, p] : nom_factors) {
        if ((f == 1) || (p == 0)) continue;
        for (int i = 1; i <= p; i++) nom *= f;
    }
    for (auto &[f, p] : denom_factors) {
        if ((f == 1) || (p == 0)) continue;
        for (int i = 1; i <= p; i++) denom *= f;
    }

    return nom / denom;
}

int main(int ac, char **av) {
    if (ac < 3) return 1;
    unsigned long long n = atol (av[1]);
    unsigned long long k = atol (av[2]);

    std::cout << Cnk(n, k) << '\n';
}
